<div class="content">
    <div class="container-narrow">
        <?=$this->view("admin/menu", ["active"=>"pages"], false)?>
        <hr>
        <form method="post" id="formas">
        <div class="row-fluid clearfix">
        	<span class="muted lead"><?=$i18n->get("admin_pages")?></span>
            <a class="btn btn-success pull-right" href="/admin/page_new"><?=$i18n->get("new_page_button")?></a>
        </div>
        <div>
        	<table class="table">
        		<thead>
        			<tr>
                        <th><?=$i18n->get("page_id")?></th>
        				<th><?=$i18n->get("page_title")?></th>
        				<th>&nbsp;</th>
                        <th>&nbsp;</th>
        			</tr>
        		</thead>
        		<?php foreach ($all_pages as $row) { ?>
        		<tr>
        			<td><?=$row['id']?></td>
        			<td><?=$row['title']?></td>
        			<td>
        				<a class="btn btn-small btn-success" href="/admin/page_edit/<?=$row['id']?>"><?=$i18n->get("page_edit")?></a>
        			</td>
                    <td>
                        <a class="btn btn-small btn-danger" href="/admin/page_remove/<?=$row['id']?>"><?=$i18n->get("page_remove")?></a>
                    </td>
        		</tr>
        		<?php }?>
        	</table>
        </div>
    </div>
</div>
