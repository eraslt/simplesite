<?php

class Admin extends AuthController {

	function Admin()	{
		parent::Controller();
		$this->authorized = false;
		if (!$this->login()) {
			$this->view("login");
			return;
		} else
			$this->authorized = true;

		$this->pages = new Model_Table("pages");
	}

	public function login() {
	    //simplest posible aut TOTO: make better one
		$passio = app_get_configuration("admin_password");
		$ussio = app_get_configuration("admin_username");
		if (@$_SESSION['cb1d3a6249c2d223c620393fa6420868']) {
			if($_SESSION['cb1d3a6249c2d223c620393fa6420868']==1)
				return true;
		}

		$pass = trim(@$_POST['password']);
		$user = trim(@$_POST['username']);
		if ($pass==$passio && $user==$ussio) {
			$_SESSION['cb1d3a6249c2d223c620393fa6420868'] = 1;
			return true;
		}
		return false;
	}

	function index() {
		$this->view("admin/main", array(
			"content" => app_get_configuration("admin_main")
		    )
		);
	}

    function pages() {
        $this->view("admin/pages", [
	       "all_pages" => (new Model_Table("pages"))->getAll()
	    ]);
	}

    function page_new() {
        $this->view("admin/page");
	}

    function page_edit($id) {
        $_id = (int)$id;
        $this->view("admin/page", [
	       "page" => (new Model_Table("pages"))->getOneRow($_id)
	    ]);
	}

    function page_remove($id) {
        $_id = (int)$id;
        (new Model_Table("pages"))->update($_id, ['removed'=>1]);
        header("Location: /admin/pages");
	}

    function langs($page = 1) {
		if(isset($_POST['lang'])) {
			foreach ($_POST["lang"] as $string_id=>$value) {
				$value = base64_decode($value);
				$value = trim($value);
				if (empty($value)) continue;
				
				$_string_id = $this->db->qstr($string_id);
				$_value = $this->db->qstr($value);
				$this->db->execute("INSERT INTO cms_lang_values (`lang_id`, `string_id`, `value`) VALUES (1, $_string_id, $_value)
  					ON DUPLICATE KEY UPDATE `value`=$_value;");
			}
		}
		$page *=1;
		$data['strings'] = $this->db->getAssoc("SELECT id, string FROM cms_lang_strings LIMIT $page,10");
		$data["default"] = $this->db->getAssoc("SELECT string_id,value FROM cms_lang_values WHERE lang_id=1");
		$data["langc"] = $this->db->getAssoc("SELECT string_id,value FROM cms_lang_values WHERE lang_id=1");
		$data["current"] = $lang_id;

		$cnt = $this->db->getOne("SELECT COUNT(*) FROM cms_lang_strings");
		$data['pag'] = paginate("/admin/langs",$page,$cnt,10);
		$this->view("admin/langs", $data);
	}

	function values() {
		if(isset($_POST['conf']) && !empty($_POST['conf'])) {
			foreach ($_POST["conf"] as $key=>$value) {
				app_save_configuration($key, trim($value));
			}
		}
		$this->view("admin/values", array("conf" => $this->db->getAssoc("SELECT `key`,`value` FROM cms_configuration ORDER BY `key`")));
	}

	function logout() {
		$_SESSION['cb1d3a6249c2d223c620393fa6420868'] = 0;
		unset($_SESSION['cb1d3a6249c2d223c620393fa6420868']);
		header("Location:?admin");
	}
}
