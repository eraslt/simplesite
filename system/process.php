<?php


require_once BASEPATH.'libraries/pagination.php';
require_once BASEPATH.'libraries/db.php';

include BASEPATH.'config'.EXT;

session_start();
	
GLOBAL $db_conf;
$db = new DB();
$db->Connect($db_conf['hostname'], $db_conf['username'], $db_conf['password'], $db_conf['database']);
$db->Execute("set names 'utf8'"); 

require_once BASEPATH.'libraries/i18n.php';
GLOBAL $i18n;
$i18n = new I18n();

require(BASEPATH.'f'.EXT);

//simple routing
$query = ltrim($_SERVER['REQUEST_URI'], '/');
$query = explode("/", $query);

if (empty($query[0])) {
    $r = explode("/", $default_route);
    $class = $r[0];
    $method = empty($r[1])?"index":$r[1];
} else {
    $class = array_shift($query);
    $method = empty($query[0])?"index":array_shift($query);
}

if (file_exists(APPPATH.'controllers/'.$class.EXT)) {
    include_once APPPATH.'controllers/'.$class.EXT;
} else {
    show_404();
}

$f = new $class();
if (!method_exists($f, "__checkAuth") || $f->__checkAuth()) {
	if (!method_exists($f, $method)) {
		show_404();
	}

	call_user_func_array(array(&$f, $method), $query);
}

//override ending
if (method_exists($f,"done"))
	call_user_func(array(&$f, done));
