<form method="post" id="formas">
<div class="marketing">
	<table class="table">
		<tr>
			<td><?=$i18n->get("year")?></td>
			<td><?=$i18n->get("month")?></td>
			<td><?=$i18n->get("day")?></td>
		</tr>
		<tr>
			<td>
				<select name="filter[year]" style="width:75px;">
					<option value=""><?=$i18n->get("filter_empty")?></option>
					<?php for ($year = 2013; $year<Date("Y")+2;$year++)  {?>
					<option <?=(($_POST['filter']['year']==$year)?"selected='selected'":"")?> value='<?=$year?>'><?=$year?></option>
					<?php }?>
				</select>
			</td>
			<td>
				<select name="filter[month]" style="width:170px;">
					<option value=""><?=$i18n->get("filter_empty")?></option>
					<?php for ($month = 1; $month<13;$month++) {?>
					<option <?=(($_POST['filter']['month']==$month)?"selected='selected'":"")?> value='<?=$month?>'>
						<?=$i18n->get("month_".$month)?>
					</option>
					<?php }?>
				</select>
			</td>
			<td>
				<select name="filter[day]" style="width:75px;">
					<option value=""><?=$i18n->get("filter_empty")?></option>
					<?php for ($day = 1; $day<32;$day++) {?>
					<option <?=(($_POST['filter']['day']==$day)?"selected='selected'":"")?> value='<?=$day?>'><?=$day?></option>
					<?php }?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2"><?=$i18n->get("status")?></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
				<select name="filter[status]" >
					<option value=""><?=$i18n->get("filter_empty")?></option>
					<?php for ($status = 0; $status<3;$status++) {?>
					<option <?=(($_POST['filter']['status']!='' && $_POST['filter']['status']==$status)?"selected='selected'":"")?> value='<?=$status?>'>
						<?=$i18n->get("reservation_status_".$status)?>
					</option>
					<?php }?>
				</select>
			</td>
			<td>
				<input type="submit" name="submit" class="btn btn-small btn-success" value="<?=$i18n->get("ieskoti")?>"/>
			</td>
		</tr>
	</table>
</div>
</form>
