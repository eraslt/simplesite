<?php

spl_autoload_register(function ($class_name) {
	$tmp = explode("_",$class_name);
	$dir = strtolower(array_shift($tmp));
	$class = strtolower(implode("_",$tmp));
	$dirs = array(
		"model"=>"models",
		"lib"=>"libraries",
	);
	if (file_exists(BASEPATH.$dirs[$dir].'/'.$class.EXT)) {
		require_once (BASEPATH.$dirs[$dir].'/'.$class.EXT);
	} else {
		throw new Exception("Unable to load $class class.");
	}
});

abstract class Controller {

	public $output = "";

	protected function view ($view, $params = array(), $to_buffer = true) {

		$_ci_path = BASEPATH.'views/'.$view.EXT;

		if (!file_exists($_ci_path)) {
			show_error("view not found");
			return;
		}
		GLOBAL $i18n;

		extract($params);

		ob_start();

		include($_ci_path);

		$buffer = ob_get_contents();
		@ob_end_clean();

		if ($to_buffer) {
		  $this->output .= $buffer;
		} else {
		    return $buffer;
		}
	}

	function Controller() {
		GLOBAL $db;
		$this->db = $db;
	}

	function __output($overide = false) {
		if ($overide)
			return $this->output;
		echo $this->output;
	}
}

abstract class AuthController extends Controller {

	protected $authorized = false;
	public function __checkAuth() {
		if ($this->authorized) return true;
		return false;
	}
}

abstract class Model {

	function Model() {
		GLOBAL $db;
		$this->db = $db;
	}

}

function show_error($heading, $status_code = 500) {
	Header("HTTP/1.1 {$status_code} evil lurks inside", TRUE, $status_code);

    echo "
        <div style='border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;'>
        <h4>$heading</h4>
        </div>";

	exit;
}
	
function show_404($page = '') {	
	show_error("404 Page Not Found", 404);
}

function app_save_configuration($key, $value) {
	GLOBAL $db;
	$_key = $db->qstr($key);
	$_value = $db->qstr($value);
	$db->Execute("DELETE FROM cms_configuration WHERE `key`=$_key");
	$db->Execute("INSERT INTO cms_configuration (`key`, `value`) VALUES ($_key, $_value)");
}

function app_get_configuration($key = null, $default = '') {
	GLOBAL $db;
	$_key = $db->qstr($key);
	$value = $db->GetOne("SELECT `value` FROM cms_configuration WHERE `key`=$_key");
	if ($value === false) {
		return $default;
	}
	return $value;
}
