<div class="masthead clearfix">
	<ul class="nav nav-pills pull-left">
		<li class="<?=(empty($active)?"active":"")?>"><a href="/admin"><?=$i18n->get("admin_main")?></a></li>
		<li class="<?=($active=="pages"?"active":"")?>"><a href="/admin/pages"><?=$i18n->get("admin_pages")?></a></li>
	</ul>
	<ul class="nav nav-pills pull-right">
		<li class="<?=($active=="values"?"active":"")?>"><a href="/admin/values"><?=$i18n->get("config_values")?></a></li>
		<li class="<?=($active=="langs"?"active":"")?>"><a href="/admin/langs"><?=$i18n->get("config_langs")?></a></li>
		<li><a href="/admin/logout"><?=$i18n->get("logout_button")?></a></li>
	</ul>
</div>
