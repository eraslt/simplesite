<?php

class Editor extends Controller {

	function Editor ()	{
		parent::__construct();
	}
	
	function index() {
		show_404();  
	}
	
	function popup($element) {
 		$this->load->view("editor/popup",array("element" => base64_decode($element)));
 		$this->__output();
 		exit;
	}
	
	function inlineCal($element) {
		require_once BASEPATH.'libraries/cal.php';
		$cal = new Calendar();
		$cal->getCalendarHtml();
		$this->load->view("editor/calendar",array("cal" => $cal->getCalendarHtml("?editor/inlineCal/aa")));
		$this->__output();
		exit;
	}
}
