<?php

class Model_Table extends Model {

	private $prefix = "cms_";

	function Model_Table($table, $prefix = false) {
		parent::Model();
		$this->setTable($table);
		if ($prefix)$this->setPrefix($prefix);
	}
	
	function setPrefix($pref) {
		$this->prefix = $pref;
	}

	function setTable($table) {
		if (empty($table))
			show_error("Model Table:unknown table");
		$this->table = $table;
	}

	function getAll($where = "") {
		return $this->db->getAll("SELECT * FROM `{$this->prefix}{$this->table}` $where");
	}
	
	function getCount($where = "") {
		return $this->db->getOne("SELECT COUNT(*) FROM `{$this->prefix}{$this->table}` $where");
	}
	
	function getRow($where = "") {
		return $this->db->getRow("SELECT * FROM `{$this->prefix}{$this->table}` $where");
	}
	
    function getOneRow($id, $primary = "id") {
		return $this->db->getRow("SELECT * FROM `{$this->prefix}{$this->table}` WHERE `$primary`=$id");
	}
	
	function getOne($field, $id, $primary = "id") {
		return $this->db->getOne("SELECT $field FROM `{$this->prefix}{$this->table}` WHERE `$primary`=$id");
	}
	
	function insert($fields) {
		$fields = $this->db->trim($this->prefix.$this->table,$fields,false);
		unset($fields['id']);
		$sql = get_insert_sql($this->prefix.$this->table, $fields);
		$this->db->execute($sql);
		return $this->db->getInsertId();
	}
	
	function update($id, $fields) {
		$fields = $this->db->trim($this->prefix.$this->table,$fields,false);
		unset($fields['id']);
		$sql = get_update_sql($this->prefix.$this->table, $fields, "id", $id);
		$this->db->execute($sql);
	}

	function remove($id, $primary = "id") {
		$this->db->execute("DELETE FROM `{$this->prefix}{$this->table}` WHERE `$primary` = $id LIMIT 1 ;");
	}

	function removeAll($id, $primary = "id") {
		$this->db->execute("DELETE FROM `{$this->prefix}{$this->table}` WHERE `$primary` = $id;");
	}
}
