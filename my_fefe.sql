-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 20, 2016 at 03:35 PM
-- Server version: 5.6.30-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `my_fefe`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_configuration`
--

CREATE TABLE IF NOT EXISTS `cms_configuration` (
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `value` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_configuration`
--

INSERT INTO `cms_configuration` (`key`, `value`) VALUES
('admin_username', 'asd'),
('site_title', 'New Tab'),
('admin_password', 'asd'),
('email_from_name', 'Registracija'),
('site_url', 'http://localhost/tool/'),
('smtp_host', ''),
('smtp_user', ''),
('smtp_pass', ''),
('email_from', 'jurgis.ostasius@gmail.com'),
('email_reply_to', ''),
('admin_main', 'fefefefe some content here');

-- --------------------------------------------------------

--
-- Table structure for table `cms_langs`
--

CREATE TABLE IF NOT EXISTS `cms_langs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `short` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cms_langs`
--

INSERT INTO `cms_langs` (`id`, `title`, `short`, `sort`) VALUES
(1, 'Lietuviškai', 'LT', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_lang_strings`
--

CREATE TABLE IF NOT EXISTS `cms_lang_strings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `string` varchar(255) NOT NULL,
  PRIMARY KEY (`id`,`string`),
  UNIQUE KEY `string` (`string`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

--
-- Dumping data for table `cms_lang_strings`
--

INSERT INTO `cms_lang_strings` (`id`, `string`) VALUES
(1, 'logout_button'),
(3, 'filter_empty'),
(4, 'mano_reservacijos'),
(5, 'new_reservation_button'),
(6, 'reservation_time'),
(7, 'reservation_date'),
(8, 'reservation_info'),
(9, 'detaliau'),
(10, 'reservation_type_0'),
(11, 'reservation_type_1'),
(12, 'reservation_type_2'),
(13, 'reservation_status_0'),
(14, 'reservation_status_1'),
(15, 'reservation_status_2'),
(16, 'new_reservation'),
(17, 'rezervuoti'),
(18, 'reservation_type_empty'),
(19, 'reservation'),
(20, 'trinti'),
(21, 'admin_reservation'),
(22, 'admin_clients'),
(23, 'config_values'),
(24, 'config_langs'),
(25, 'year'),
(26, 'month'),
(27, 'day'),
(28, 'status'),
(29, 'client'),
(30, 'new_client_button'),
(31, 'filtras'),
(32, 'ieskoti'),
(33, 'client_username'),
(34, 'client_contact'),
(35, 'username'),
(36, 'contacts'),
(37, 'email'),
(38, 'password'),
(39, 'blocked'),
(40, 'admin_username'),
(41, 'admin_password'),
(42, 'site_title'),
(43, 'site_url'),
(44, 'smtp_host'),
(45, 'smtp_user'),
(46, 'smtp_pass'),
(47, 'email_from'),
(48, 'email_reply_to'),
(49, 'login_header'),
(50, 'login_button'),
(51, 'month_1'),
(52, 'month_2'),
(53, 'month_3'),
(54, 'month_4'),
(55, 'month_5'),
(56, 'month_6'),
(57, 'month_7'),
(58, 'month_8'),
(59, 'month_9'),
(60, 'month_10'),
(61, 'month_11'),
(62, 'month_12'),
(63, 'reservations'),
(64, 'save'),
(65, 'new_client'),
(66, 'patvirtinti'),
(67, 'atsaukti'),
(68, 'ar_trinti'),
(69, 'password_leave_empty'),
(70, 'field_bad_simbols'),
(71, 'field_required'),
(72, 'field_bad_email'),
(73, 'username_uzimtas'),
(74, 'rezervation_accepted_subject'),
(75, 'rezervation_accepted_body'),
(76, 'email_from_name'),
(77, 'rezervation_canceled_subject'),
(78, 'rezervation_canceled_body'),
(79, 'empty_value'),
(80, 'user_clients'),
(81, 'add_new'),
(82, 'caption'),
(83, 'code'),
(84, 'vat'),
(85, 'contact'),
(86, 'phone'),
(87, 'www'),
(88, 'info'),
(89, 'selected_days_taken');

-- --------------------------------------------------------

--
-- Table structure for table `cms_lang_values`
--

CREATE TABLE IF NOT EXISTS `cms_lang_values` (
  `lang_id` int(11) NOT NULL,
  `string_id` int(11) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`lang_id`,`string_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_lang_values`
--

INSERT INTO `cms_lang_values` (`lang_id`, `string_id`, `value`) VALUES
(1, 1, 'Logout'),
(1, 3, '----------'),
(1, 50, 'Login'),
(1, 49, 'Login'),
(1, 4, 'My reservations'),
(1, 5, '+New reservation'),
(1, 6, 'Reserved on'),
(1, 7, 'Reservation date'),
(1, 8, 'Info'),
(1, 9, 'More'),
(1, 10, 'Full residence'),
(1, 11, 'First housing'),
(1, 12, 'Second housing'),
(1, 13, 'New'),
(1, 14, 'Confirmed'),
(1, 15, 'Canceled'),
(1, 16, 'New reservation'),
(1, 17, 'Make reservation'),
(1, 18, '--------'),
(1, 19, 'Reservation'),
(1, 20, 'Delete'),
(1, 21, 'Reservations'),
(1, 22, 'Clients'),
(1, 23, 'Config'),
(1, 24, 'Translations'),
(1, 25, 'Year'),
(1, 26, 'Month'),
(1, 27, 'Day'),
(1, 28, 'Status'),
(1, 29, 'Client'),
(1, 30, 'New client'),
(1, 31, 'Search'),
(1, 32, 'Search'),
(1, 33, 'Username'),
(1, 34, 'Contacts'),
(1, 35, 'Username'),
(1, 36, 'Contacts'),
(1, 37, 'E-mail'),
(1, 38, 'Password'),
(1, 39, 'Blocked'),
(1, 40, 'Admin username'),
(1, 41, 'Admin password'),
(1, 42, 'Site title'),
(1, 43, 'Site URL'),
(1, 44, 'SMTP host'),
(1, 45, 'SMTP username'),
(1, 46, 'SMTP password'),
(1, 47, 'E-mail "From"'),
(1, 48, 'E-mail "Reply-to"'),
(1, 51, 'January'),
(1, 52, 'February'),
(1, 53, 'March'),
(1, 54, 'April'),
(1, 55, 'May'),
(1, 56, 'June'),
(1, 57, 'July'),
(1, 58, 'August'),
(1, 59, 'September'),
(1, 60, 'October'),
(1, 61, 'November'),
(1, 62, 'December'),
(1, 63, 'Reservations'),
(1, 64, 'Save/add'),
(1, 65, 'New client'),
(1, 66, 'Confirm'),
(1, 67, 'Cancel'),
(1, 68, 'Are you sure?'),
(1, 69, 'Norėdami pakeisti, įrašykite nauja spaltažodi, priešingu atveju, palikite tuščią.'),
(1, 70, 'Neleistini simboliai. Leidžiama tik raidės ir skaičai.'),
(1, 71, 'Privalomas laukas.'),
(1, 72, 'Blogas el. pašto adresas'),
(1, 73, 'Toks vartotojo prisijungimo vardas užimtas.'),
(1, 76, 'E-mail "From name"'),
(1, 74, 'Registracija patvirtinta'),
(1, 75, '<p>Registracija buvo patvirtinta.</p>'),
(1, 77, 'Registracija atšaukta'),
(1, 78, '<p>Registracija buvo at&scaron;aukta.</p>'),
(1, 79, 'No value'),
(1, 82, 'Client'),
(1, 83, 'Code'),
(1, 84, 'Vat number'),
(1, 85, 'Contact person'),
(1, 86, 'Phone'),
(1, 87, 'Website'),
(1, 88, 'Additional info'),
(1, 80, 'Client clients'),
(1, 81, 'Add new');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE IF NOT EXISTS `cms_pages` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `order` smallint(6) NOT NULL DEFAULT '0',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `removed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `title`, `content`, `order`, `blocked`, `removed`) VALUES
(8, 'Apie mus', '<html>\r\n	<head>\r\n		<title></title>\r\n	</head>\r\n	<body>\r\n		<p>\r\n			Informacija ruo&scaron;iama</p>\r\n	</body>\r\n</html>\r\n', 0, 0, 0),
(9, 'Paslaugos', '<html>\r\n	<head>\r\n		<title></title>\r\n	</head>\r\n	<body>\r\n		<p>\r\n			Informacija ruo&scaron;iama</p>\r\n	</body>\r\n</html>\r\n', 0, 0, 0),
(10, 'Noriu parduoti', '<html>\r\n	<head>\r\n		<title></title>\r\n	</head>\r\n	<body>\r\n		<p>\r\n			Informacija ruo&scaron;iama</p>\r\n	</body>\r\n</html>\r\n', 0, 0, 0),
(11, 'Ieškau pirkti', '<html>\r\n	<head>\r\n		<title></title>\r\n	</head>\r\n	<body>\r\n		<p>\r\n			Informacija ruo&scaron;iama</p>\r\n	</body>\r\n</html>\r\n', 0, 0, 0),
(12, 'Kontaktai', '<html>\r\n	<head>\r\n		<title></title>\r\n	</head>\r\n	<body>\r\n		<p>\r\n			Informacija ruo&scaron;iama</p>\r\n	</body>\r\n</html>\r\n', 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
