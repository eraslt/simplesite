<div class="content">
    <div class="container-narrow">
        <?=$this->view("admin/menu", ["active"=>"pages"], false)?>
        <hr>
        <form method="post" id="formas">
        <?php if($id) { ?>
        <input type="hidden" name="page[id]"  value="<?=$id?>">
        <?php } ?>
        <div class="row-fluid">
        	<h3 class="muted"><?=(empty($username))?$i18n->get("new_client"):$i18n->get("client_username")." - ".$username?></h3>
        </div>
        <div class="marketing">
        	<table class="table">
        		<tr>
        			<td><?=$i18n->get("page_title")?></td>
        			<td><input type="text" name="page[title]"  value="<?=$title?>"></td>
        			<td>
        				<?php if($errors['title']) { ?>
        				<span style="color:red"><?=$i18n->get($errors['title'])?></span>
        				<?php } else {?>
        				&nbsp;
        				<?php }?>
        			</td>
        		</tr>
        		<tr>
        			<td><?=$i18n->get("page_content")?></td>
        			<td><textarea name="page[content]"><?=$content?></textarea></td>
        			<td>
        				<?php if($errors['content']) { ?>
        				<span style="color:red"><?=$i18n->get($errors['content'])?></span>
        				<?php } else {?>
        				&nbsp;
        				<?php }?>
        			</td>
        		</tr>
        		<tr>
        			<td><?=$i18n->get("page_order")?></td>
        			<td><input type="text" name="page[order]" value="<?=$order?>" autocomplete="off"></td>
        			<td>
        				<?php if($errors['order']) { ?>
        				<span style="color:red"><?=$i18n->get($errors['order'])?></span>
        				<?php } else {?>
        				&nbsp;
        				<?php }?>
        			</td>
        		</tr>
        		<tr>
        			<td><?=$i18n->get("page_blocked")?></td>
        			<td><input type="checkbox" name="client[blocked]" value="on" <?=($blocked?"checked":"")?>></td>
        			<td>&nbsp;</td>
        		</tr>
        		<tr>
        			<td>
        				&nbsp;
        			</td>
        			<td>
        				<input type="submit" name="submit" class="btn btn-small btn-success" value="<?=$i18n->get("save")?>"/>
        			</td>
        			<td>&nbsp;</td>
        		</tr>
        	</table>
        </div>
        </form>
    </div>
</div>