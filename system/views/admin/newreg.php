<div class="row-fluid">
	<h3 class="muted"><?=$i18n->get("new_reservation")?></h3>
</div>
<div class="clearfix">
	<div class="pull-left">
		<?=$cal?>
	</div>
	<div class="pull-left" style="padding-left:50px;">
		<form method="post" id="formas">
		<div>
			<input type="hidden" value="<?=$date?>" name="res[date]" />
			<span class="muted"><?=$date?></span>
		</div>
		<div>
			<select name="res[user_id]" id="tr27" class="">
				<?php foreach ($clients as $id=>$client) {?>
				<option value="<?=$id?>"><?=$client?></option>
				<?php }?>
			</select>
		</div>
		<div>
			<select name="res[type]" id="ie16" class="">
				<option selected="selected" value=""><?=$i18n->get("reservation_type_empty")?></option>
				<?php if (empty($res)) {?>
				<option value="0"><?=$i18n->get("reservation_type_0")?></option>
				<?php } if (!$res[1]) {?>
				<option value="1"><?=$i18n->get("reservation_type_1")?></option>
				<?php } if (!$res[2]) {?>
				<option value="2"><?=$i18n->get("reservation_type_2")?></option>
				<?php }?>
			</select>
		</div>
		<div>
			<a class="btn btn-small btn-success" href="javascript:void(0)" onclick="fomt_submit();"><?=$i18n->get("rezervuoti")?></a>
		</div>
		</form>
	</div>
</div>
<script type="text/javascript">
function fomt_submit() {
	var el = document.getElementById('tr27');
	if (!el.value) {
		if (el.className.indexOf("error")!=false)  
			el.className += "error";
		return false;
	}
		el = document.getElementById('ie16');
	if (!el.value) {
		if (el.className.indexOf("error")!=false)  
			el.className += "error";
		return false;
	}
	document.getElementById('formas').submit();	
}
</script>
