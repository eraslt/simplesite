<script type="text/javascript">
onchange = function(id, val) {
	document.getElementById(id).value = val.trim();
	document.getElementById(id+'text').innerHTML = val.trim();
	document.getElementById("values_formas").submit();
};

change = function(id) {
	var el = document.getElementById(id);
	var val = prompt("New value",el.value);
	onchange(id, val);
};
</script>
<div class="content">
    <div class="container-narrow">
        <?=$this->view("admin/menu", array("active"=>"values"), false)?>
        <hr>
        <div  style="max-width:700px;overflow: hidden;">
        <form method="post" id="values_formas">
        <table class="table">
        	<tr>
        		<th width="30%">Key</th>
        		<th>Value</th>
        	</tr>
        	<?php foreach($conf as $key => $value) {?>
        	<tr>
        		<td><?=$i18n->get($key)?></td>
        		<td>
        			<input type="hidden" name="conf[<?=$key?>]" id="lang[<?=$key?>]" value="<?=$value?>">
        			<?php $red = ""; if (empty($value)) {$value=$i18n->get("empty_value");$red="color:red;";}?>
        			<a href="javascript:void(0)" onclick="change('lang[<?=$key?>]')" id="lang[<?=$key?>]text" style="<?=$red?>" /><?=$value?></a>
        		</td>
        	</tr>
        	<?php } ?>
        </table>
        </form>
        </div>
    </div>
</div>
