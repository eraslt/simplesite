<?php

class Content extends Controller {

	function Content ()	{
		parent::__construct();
	}
	
	function index() {
		show_404();  
	}

	function page($id) {
	    $id = (int)$id;
	    $this->view("content/page", [
	       "all_pages" => (new Model_Table("pages"))->getAll(),
	       "page" => (new Model_Table("pages"))->getOneRow($id),
	    ]);
	    /*$content = $this->__output(true);
	    $f = fopen("static/page{$id}.html","w+");
	    fwrite($f, $content);
	    fclose($f);
	    header("Location: /static/page{$id}.html", false, 302);
 		exit;*/
	}

	public static function getUrl($id, $type="page") {
	    $nice_name = "";
	    switch ($type) {
	        case "blog":
	            $nice_name =(new Model_Table("blog"))->getOne("title", $id);
	            $type = "b";
                break;
	        default:
                $nice_name =(new Model_Table("pages"))->getOne("title", $id);
                $type = "p";
                break;
	    }
	    $nice_name = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $nice_name);
	    $nice_name = str_replace(' ', '_', $nice_name);
	    return $nice_name."_$type$id.html";
	}
}
