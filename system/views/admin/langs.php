<script type="text/javascript" src="template/js/base64.js"></script>
<script type="text/javascript">

makeEditor = function (id) {
	newwindow = window.open('<?php echo SITE_URL?>/?editor/popup/'+Base64.encode(id),'editor','height=600,width=800');
	if (window.focus) {newwindow.focus();}
	return false;
};

onchange = function(id, val) {
	document.getElementById(id).value = Base64.encode(val.trim());
	document.getElementById(id+'text').innerHTML = val.trim();
	document.getElementById("formas").submit();
};

change = function(id) {
	var el = document.getElementById(id);
	var val = prompt("New value",Base64.decode(el.value));
	onchange(id, val);
};
</script>
<div class="content">
    <div class="container-narrow">
        <?=$this->view("admin/menu", array("active"=>"langs"), false)?>
        <hr>
        <form id="formas" method="post">
     	<?=$pag?>
        <table class="table">
        	<tr>
        		<th width="30%">String</th>
        		<th width="30%">Value</th>
        		<th width="30%">Translation</th>
        	</tr>
        	<?php foreach($strings as $string_id => $string) {?>
        	<tr>
        		<td><?=$string?></td>
        		<td><?=$default[$string_id]?></td>
        		<td>
        			<input type="hidden" name="lang[<?=$string_id?>]" id="lang[<?=$string_id?>]" value="<?=base64_encode($langc[$string_id])?>">
        			<a href="javascript:void(0)" onclick="change('lang[<?=$string_id?>]')"/>Plain</a>,&nbsp;
        			<a href="javascript:void(0)" onclick="makeEditor('lang[<?=$string_id?>]')"/>HTML</a><br/>
        			<span id="lang[<?=$string_id?>]text">
        			<?=$langc[$string_id]?>
        			</span>
        		</td>
        	</tr>
        	<?php } ?>
        </table>
        </form>
    </div>
</div>